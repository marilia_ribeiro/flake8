print('hello world')

#erro comentário colado no hashtag - E265 block comment should start with '# '

# passou - 79 caracteres
print('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh')

# não passou 80 caracteres - E501 line too long (80 > 79 characters)
# para ignorar o erro E501 basta executar: flake8 --ignore E501 hello.py
print('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh')
