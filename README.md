## Getting Started - Flake8

Seguindo o tutorial da documentação do flake8 latest. Disponível na url: http://flake8.pycqa.org/en/latest/index.html

## Instalando dependências

- Crie uma virtualenv com python3 utilizando o seguinte comando: `python3 -m venv nome_venv`
- Ative a virtualenv `nome_venv`: `. nome_venv/bin/activate`

- Instale as dependências contidas no arquivo requirements.txt: `pip install -r requirements.txt`

## Executando o flake8
- Com a virtualenv ativa, execute o servidor: `flake8 nome_arquivo.py`
